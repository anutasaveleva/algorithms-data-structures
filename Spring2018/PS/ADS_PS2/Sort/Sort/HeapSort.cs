﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeapSort
{
    class Program
    { 
        static void Main(string[] args)
        {
            PerformSort("reversed65K.txt");
            PerformSort("sorted65K.txt");
            PerformSort("random65K.txt");
            PerformSort("almostSorted65K.txt");
        }

        static void PerformSort(string filename)
        {
            var timer = new Stopwatch();
            var heap = new HeapSort();
            var arr = File.ReadAllText($"{filename}").Split(' ').Select(x => int.Parse(x)).ToList();
            timer.Start();
            var res = arr.OrderBy(x => x).First();
            timer.Stop();
            Console.WriteLine(filename.Remove(filename.Length - 4));
            Console.WriteLine($"Ordinary sort time={timer.ElapsedMilliseconds}"); 
            timer.Restart();
            heap.HeapSortMethod(arr);
            timer.Stop();
            Console.WriteLine($"Time in milliseconds={timer.ElapsedMilliseconds}");
            ..Console.WriteLine($"Iterations Count:{heap.Iterations}");
            Console.WriteLine();
           //Console.WriteLine(string.Join(" ",arr));
        }
    }

    public class HeapSort
    {
        public int HeapSize { get; private set; }
        public int Iterations { get; private set; }
        public void HeapSortMethod(List<int> arr)
        {
            Iterations = 0;
            HeapSize = arr.Count - 1;
            for (int p = HeapSize/ 2; p >= 0; p--)
                Heapify(arr, p);
            for (int i = arr.Count - 1; i >= 0; i--)
            {
                Swap(arr, 0, i);
                HeapSize--;
                Heapify(arr, 0);
            }
        }
        public void Heapify(List<int> arr,int index)
        {
            var left = 2 * index + 1;
            var right = left + 1;
            var idxOfLargest = index;
            if (left <= HeapSize && arr[left] > arr[idxOfLargest])
                idxOfLargest = left;
            if (right <= HeapSize && arr[right] > arr[idxOfLargest])
                idxOfLargest = right;
            if (idxOfLargest!=index)
            {
                Swap(arr, index, idxOfLargest);
                Heapify(arr, idxOfLargest);
            }
        }
        public void Swap(List<int> arr, int idx1,int idx2)
        {
            //Число итераций - количество операций по смене двух элементов дерева.
            Iterations++;
            var tmp = arr[idx1];
            arr[idx1] = arr[idx2];
            arr[idx2] = tmp;
        }
        
    }
}
