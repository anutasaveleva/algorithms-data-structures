﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroSort
{
    class Program
    {
        public static int Iterations { get; set; }
        public static bool NotYetSorted { get; set; }
        static void Main(string[] args)
        {
            PerformSort("AlmostSorted50K.txt");
            PerformSort("Reversed50K.txt");
            PerformSort("Sorted50K.txt");
            PerformSort("Random50K.txt");
            PerformSort("SmallData100.txt");
        }
        
        static void PerformSort(string filename)
        {
            NotYetSorted = false;
            var timer = new Stopwatch();
            var arr = File.ReadAllText($"{filename}").Split(' ').Select(x => int.Parse(x)).ToArray();
            timer.Start();
            var res = arr.OrderBy(x => x).First();
            timer.Stop();
            Console.WriteLine(filename.Remove(filename.Length - 4));
            Console.WriteLine($"Ordinary sort time={timer.ElapsedMilliseconds}");            
            timer.Restart();
            QuickSort.QuickSorting(arr, 0, arr.Length - 1);
            if (NotYetSorted)
            {
                HeapSort.SortMethod(arr);
            }
            timer.Stop();
            Console.WriteLine($"Time in milliseconds={timer.ElapsedMilliseconds}");
            Console.WriteLine($"Iterations Count={Iterations}");
            Console.WriteLine();
        }

        public static void Swap(int[] arr, int idx1, int idx2)
        {
            var tmp = arr[idx1];
            arr[idx1] = arr[idx2];
            arr[idx2] = tmp;
        }
        class QuickSort
        {
            public static void QuickSorting(int[] arr, int left, int right)
            {
                Iterations++;
                var pivot = right;
                var j = left;
                for (int i = left; i <= right - 1; i++)
                {
                    if (arr[i] <= arr[pivot])
                    {
                        Swap(arr, i, j);
                        j++;
                    }
                }
                Swap(arr, j, right);
                var x = 2 * Math.Log(arr.Length,2);
                if (Iterations >=x )
                {
                    NotYetSorted = true;//Для перехода на пирамидальную сортировку.
                    return;
                }
                else
                {
                    if (j > left) QuickSorting(arr, left, j - 1);
                    if (j < right) QuickSorting(arr, j + 1, right);
                }
            }
        }

        class HeapSort
        {
            public static int HeapSize { get; private set; }
            static void Heapify(int[] arr, int index)
            {
                Iterations++;
                var left = 2 * index + 1;
                var right = left + 1;
                var idxOfLargest = index;
                if (left <= HeapSize && arr[left] > arr[idxOfLargest])
                    idxOfLargest = left;
                if (right <= HeapSize && arr[right] > arr[idxOfLargest])
                    idxOfLargest = right;
                if (idxOfLargest != index)
                {
                    Swap(arr, index, idxOfLargest);
                    Heapify(arr, idxOfLargest);
                }
            }

            public static void SortMethod(int[] arr)
            {
                HeapSize = arr.Length - 1;
                for (int i = HeapSize / 2; i >= 0; i--)
                    Heapify(arr, i);

                for (int i = arr.Length - 1; i >= 0; i--)
                {
                    Swap(arr, 0, i);
                    HeapSize--;
                    Heapify(arr, 0);
                }
            }
        }
    }
}
