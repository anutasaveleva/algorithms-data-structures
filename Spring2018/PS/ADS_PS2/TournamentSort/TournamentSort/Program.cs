﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TournamentSort
{
    class Program
    {
        class Item
        {
            public int Index { get; set; }
            public bool Active { get; set; }
            public int Data { get; set; }
            public static bool operator >(Item item1, Item item2) => item1.Data > item2.Data;
            public static bool operator <(Item item1, Item item2) => item1.Data < item2.Data;

        }

        static void Main(string[] args)
        {
            PerformSort("reversed65K.txt");
            PerformSort("sorted65K.txt");
            PerformSort("random65K.txt");
            PerformSort("almostSorted65K.txt");
        }

        public static int Iterations { get; set; }

        static void TournamentSort(int[] arr)
        {
            Iterations = 0;
            var n = arr.Length;
            int treeSize = 2 * n - 1;
            var tree = new Item[treeSize];
            var index = treeSize - n;
            var j = 0;
            for (int i = index; i < treeSize; i++)
            {
                var item = new Item();
                item.Index = i;
                item.Data = arr[j++];
                item.Active = true;
                tree[i] = item;
            }
            var z = index;
            while (z > 0)
            {
                j = z;
                z = z%4==0 ? (j - 1) / 2 : j/2;
                while (j+1 < treeSize && (j - 1) >= 0)
                {

                    tree[(j - 1) / 2] = (!tree[j + 1].Active || tree[j] < tree[j + 1]) ?
                    tree[j] : tree[j + 1];
                    j += 2;
                }
            }

            for (var i = 0; i < n - 1; i++)
            {
                arr[i] = tree[0].Data;
                tree[0].Active = false;
                UpdateTree(tree, tree[0].Index);
            }
            arr[arr.Length - 1] = tree[0].Data;
        }

        static void UpdateTree(Item[] tree, int idx)
        {
            Iterations++;
            if (idx % 2 == 0)
                tree[(idx - 1) / 2] = tree[idx - 1];
            else
                tree[(idx - 1) / 2] = tree[idx + 1];
            idx = (idx - 1) / 2;
            var j = 0;
            while (idx > 0)
            {
                 j=idx % 2 == 0 ?
                    idx - 1 :  idx + 1;
                if (!tree[idx].Active || !tree[j].Active)
                    if (tree[idx].Active)
                        tree[(idx - 1) / 2] = tree[idx];
                    else
                        tree[(idx - 1) / 2] = tree[j];
                else
                    tree[(idx - 1) / 2] = (tree[idx] < tree[j]) ? tree[idx] : tree[j];
                idx = (idx - 1) / 2;
            }
        }
        
        static void PerformSort(string filename)
        {
            var timer = new Stopwatch();
            var arr = File.ReadAllText($"{filename}").Split(' ').Select(x => int.Parse(x)).ToArray();
            timer.Start();
            var res = arr.OrderBy(x => x).First();
            timer.Stop();
            Console.WriteLine(filename.Remove(filename.Length - 4));
            Console.WriteLine($"Ordinary sort time={timer.ElapsedMilliseconds}");
            timer.Restart();
            TournamentSort(arr);
            timer.Stop();
            Console.WriteLine($"Time in milliseconds={timer.ElapsedMilliseconds}");
            Console.WriteLine();
        }
    }
}
