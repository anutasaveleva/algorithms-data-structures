﻿using System;
using System.IO;
using System.Linq;

namespace Treap
{
    public class TreapClass
    {
        public int x;//tree index
        public int y;//heap index, priority
        public TreapClass left;
        public TreapClass right;
        public TreapClass() { }
        public TreapClass(int root, int priority, TreapClass leftChild, TreapClass rightchild)
        {
            x = root;
            y = priority;
            left = leftChild;
            right = rightchild;
        }
        public TreapClass(int root, int priority)
        {
            x = root;
            y = priority;
        }
    }

    public class CartesianTree
    {
        public TreapClass root;
        public CartesianTree(TreapClass node)
        {
            root = node;
        }
        public CartesianTree(int[] data)
        {
            root = Build(data);
        }

        static TreapClass Build(int[] data)
        {
            if (data == null || data.Length == 0)
                return null;
            var rnd = new Random();
            var randomData = new int[data.Length];
            for (int i = 0; i < randomData.Length; i++)
                randomData[i] = rnd.Next();
            var arr = new Tuple<int, int>[data.Length];
            for (int i = 0; i < data.Length; i++)
                arr[i] = new Tuple<int, int>(data[i], randomData[i]);
            arr = arr.OrderBy(x => x.Item1).ToArray();
            return Build(arr, 0, data.Length);
        }

        static TreapClass Build(Tuple<int, int>[] data, int start, int finish)
        {
            if (start >= finish)
                return null;

            var max = int.MinValue;
            var maxIdx = -1;
            var maxPrior = int.MinValue;
            for (int i = start; i < finish; i++)
            {
                var currentPriority = data[i].Item2;
                if (currentPriority > maxPrior)
                {
                    max = data[i].Item1;
                    maxPrior = currentPriority;
                    maxIdx = i;
                }
            }
            var current = new TreapClass(max, maxPrior);
            current.left = Build(data, start, maxIdx);
            current.right = Build(data, maxIdx + 1, finish);
            return current;
        }

        public void Split(TreapClass node, int x, out TreapClass L, out TreapClass R)
        {
            TreapClass newTree = null;
            if (node.x <= x)
            {
                if (node.right == null)
                    R = null;
                else
                    Split(node.right, x, out newTree, out R);
                L = new TreapClass(node.x, node.y, node.left, newTree);
            }
            else
            {
                if (node.left == null)
                    L = null;
                else
                    Split(node.left, x, out L, out newTree);
                R = new TreapClass(node.x, node.y, newTree, node.right);
            }
        }

        public TreapClass Add(int value)
        {
            TreapClass left, right;
            var m = new CartesianTree(new int[] { value });
            Split(root, value, out left, out right);
            return Program.Merge(Program.Merge(left, m.root), right);
        }

        public TreapClass Remove(int x)
        {
            var cur = root;
            while (cur.x != x)
            {
                if (cur.x < x)
                    cur = cur.right;
                else cur = cur.left;
            }
            return Program.Merge(cur.left, cur.right);
        }
    }

    public class Program
    {
        //keys of l are lower than keys of r
        public static CartesianTree Merge(CartesianTree left, CartesianTree right)
        {
            return new CartesianTree(Merge(left.root, right.root));
        }

        public static TreapClass Merge(TreapClass L, TreapClass R)
        {
            if (L == null) return R;
            if (R == null) return L;

            if (L.y > R.y)
            {
                var newRight = Merge(L.right, R);
                return new TreapClass(L.x, L.y, L.left, newRight);
            }
            else
            {
                var newLeft = Merge(L, R.left);
                return new TreapClass(R.x, R.y, newLeft, R.right);
            }
        }

        static void Main(string[] args)
        {
            var rnd = new Random();
            var array = File.ReadAllText($"input.txt").Split(' ').Select(x => int.Parse(x)).ToArray();
            var t = new CartesianTree(array);
            var a = new TreapClass();
            var b = new TreapClass();
            t.Split(t.root, 3, out a, out b);
            var t2 = Merge(a, b);
        }
    }
}