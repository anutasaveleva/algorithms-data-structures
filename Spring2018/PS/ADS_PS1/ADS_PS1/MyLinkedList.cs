﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADS_PS1
{
    public class MyLinkedList : IEnumerable<Segment>
    {
        public int Count { get; private set; }
        public Segment First { get; private set; }
        public Segment Last { get; private set; }
        public void Add(Segment data)
        {
            if (First == null)
            {
                First = data;
            }
            else
                Last.Next = data;
            Last = data;
            Count++;
        }

        public IEnumerator<Segment> GetEnumerator()
        {
            var current = First;
            var count = 0;
            while (current != null && count++ != Count)
            {
                yield return current;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
