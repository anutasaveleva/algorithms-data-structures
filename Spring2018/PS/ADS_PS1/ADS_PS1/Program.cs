﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ADS_PS1
{
    public class Program
    {
        static void Main(string[] args)
        {
            var file = File.ReadAllText("data.txt");
            var i = new SegmentList(file);
            i.Show();
            i.Sort();
            Console.WriteLine("List after sorting");
            i.Show();
            Console.WriteLine("List after inserting");
            i.Insert(new Segment() { x1 = 5, y1 = 5, x2 = 3, y2 = 4 });
            i.Show();            
            
            Console.WriteLine("LengthList");
            i.LengthList(1, 2).Show();
            Console.WriteLine("AngleList");
            i.AngleList().Show();
        }

        public static string LengthToString(SegmentList list)
        {
            StringBuilder s = new StringBuilder();
            foreach (var el in list.items)
                s.Append($"{el.x1} {el.y1} {el.x2} {el.y2} {el.Length} \n");
            return s.ToString();
        }
    }

    public class SegmentList
    {
        public MyLinkedList items;
        public SegmentList()
        {
            items = new MyLinkedList();
        }
        public SegmentList(string filename)
        {
            var arr = filename.Split(' ');
            if (arr.Length % 4 != 0) throw new Exception("Not enough points to build a list");
            items = new MyLinkedList();
            var i = 0;
            while (i + 4 <= arr.Length)
            {
                var seg = new Segment
                {
                    x1 = int.Parse(arr[i]),
                    y1 = int.Parse(arr[i + 1]),
                    x2 = int.Parse(arr[i + 2]),
                    y2 = int.Parse(arr[i + 3])
                };
                i += 4;
                items.Add(seg);
            }
        }

        public void Show()
        {
            Console.WriteLine(ToString(items));
        }

        public string ToString(MyLinkedList segList)
        {
            StringBuilder a = new StringBuilder();
            foreach (var segment in segList)
                a.Append($"x1={segment.x1}, y1={segment.y1}, x2={segment.x2}, y2={segment.y2}, length={segment.Length} \n");
            return a.ToString();
        }

        public void Insert(Segment f)
        {
            bool flag = true;
            foreach (var seg in items)
            {
                if (SegEquals(seg, f))
                {
                    flag = false;
                    break;
                }
            }
            if (flag)
                items.Add(f);
        }

        static bool SegEquals(Segment a, Segment b)
        {
            return a.x1 == b.x1 && a.x2 == b.x2 && a.y1 == b.y1 && a.y2 == b.y2;
        }

        public SegmentList AngleList()
        {
            var anglelist = new SegmentList();
            foreach (var item in items)
            {
                if ((Math.Abs(item.DeltaX / item.Length - Math.Cos(Math.PI / 6)) < 1e-7 ||
                    Math.Abs(item.DeltaX / item.Length - Math.Cos(Math.PI / 4)) < 1e-7) && item.DeltaX * item.DeltaY != 0)
                    anglelist.items.Add(item);
            }
            return anglelist;
        }

        public SegmentList LengthList(int a, int b)
        {
            var lengthList = new SegmentList();
            foreach (var elem in items)
                if (elem.Length >= a && elem.Length <= b)
                    lengthList.items.Add(elem);
            return lengthList;
        }

        public void Sort()
        {
            var head = items.First;
            items = new MyLinkedList();
            while (head != null)
            {
                items.Add(Mergesort(head));
                head = head.Next;
            }
        }

        private Segment Mergesort(Segment head)
        {
            if (head == null || head.Next == null || head.Next.Next == null) { return head; }
            else
            { Segment middle = GetMiddle(head);
                Segment secHalf =middle.Next;
                middle.Next = null;

                return Merge(Mergesort(head), Mergesort(secHalf));
            }
        }

        private Segment Merge(Segment a, Segment b)
        {
            var currentHead = new Segment();
            var current = currentHead;
            while (a != null && b != null)
            {
                if (a.Length <= b.Length) { current.Next = a; a = a.Next; }
                else { current.Next = b; b = b.Next; }
                current = current.Next;
            }
            current.Next = a ?? b;
            return currentHead.Next;
        }
        private Segment GetMiddle(Segment head)
        {
            if (head == null) { return head; }
            Segment midFinder; Segment cursor; midFinder = cursor = head;
            while (cursor.Next != null && cursor.Next.Next != null)
            {
                midFinder = midFinder.Next;
                cursor = cursor.Next.Next;
            }
            return midFinder;
        }
    }
}
