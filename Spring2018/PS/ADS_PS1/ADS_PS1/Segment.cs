﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADS_PS1
{
    public class Segment
    {
        public int x1;
        public int y1;
        public int x2;
        public int y2;
        public int DeltaX
        {
            get { return Math.Abs(x1 - x2); }
        }
        public int DeltaY
        {
            get { return Math.Abs(y1 - y2); }
        }
        public double Length
        {
            get { return Math.Sqrt(DeltaX * DeltaX + DeltaY * DeltaY); }
        }
        public Segment Next;
    }
}
