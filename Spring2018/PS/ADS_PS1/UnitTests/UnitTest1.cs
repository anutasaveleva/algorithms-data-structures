﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ADS_PS1;

namespace UnitTests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void InsertTest()
        {
            var i = new SegmentList();
            i.Insert(new Segment() { x1 = 1, y1 = 9, x2 = 4, y2 = 7 });
            var expList = $"x1=1, y1=9, x2=4, y2=7, length={Math.Sqrt((4 - 1) * (4 - 1) + (9 - 7) * (9 - 7))} \n";
            Assert.AreEqual(i.ToString(i.items), expList);
        }

        [TestMethod]
        public void AngleListTest()
        {
            var list = new SegmentList("1 2 1 3 1 2 1 4 5 9 4 8 -3 -2 0 2");
            var s = Program.LengthToString(list.AngleList());
            var len1 = Math.Sqrt((5 - 4) * (5 - 4) + (9 - 8) * (9 - 8));
            Assert.AreEqual(s, $"5 9 4 8 {len1} \n");
        }

        [TestMethod]
        public void StringForShowMethod()
        {
            var a = new SegmentList("1 2 1 4");
            Assert.AreEqual(a.ToString(a.items), "x1=1, y1=2, x2=1, y2=4, length=2 \n");
        }

        [TestMethod]
        public void LengthListTest()
        {
            var a = new SegmentList("0 2 3 4 7 9 5 -3 0 0 6 9");
            var len1 = Math.Sqrt((4 - 2) * (4 - 2) + 3 * 3);
            Assert.AreEqual(Program.LengthToString(a.LengthList(2, 5)), $"0 2 3 4 {len1} \n");
        }

        [TestMethod]
        public void SortTest()
        {
            var i = new SegmentList("1 2 1 3 1 2 1 4");
            i.Sort();
            string expSortedList = string.Join("\n",
                new[]{ "x1=0, y1=0, x2=0, y2=0, length=0 ",
                $"x1=1, y1=2, x2=3, y2=4, length={Math.Sqrt((2-1)*(2-1)+(3-4)*(3-4))} "});
            string s = null;
            foreach (var a in i.items)
            {
                s += $"x1={a.x1}, y1={a.y1}, x2={a.x2}, y2={a.y2}, length={a.Length} \n";
            }
            Assert.AreEqual(i.ToString(i.items), expSortedList);
        }
    }
}
